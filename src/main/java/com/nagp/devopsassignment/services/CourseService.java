package com.nagp.devopsassignment.services;

import java.util.List;

import com.nagp.devopsassignment.entities.Course;

public interface CourseService {

	public List<Course> getCourses();
	
	public Course getCourse(long courseId);

	public Course addCourse(Course course);
	
}
