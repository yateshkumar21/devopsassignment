package com.nagp.devopsassignment.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.nagp.devopsassignment.entities.Course;

public interface CourseDao extends JpaRepository<Course, Long>{

}
