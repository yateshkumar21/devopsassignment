# To run the application standalone 

### Generate jar file  
mvn clean install 

mvn clean package

### Spin Application and MyqSQL Database container
docker-compose build && docker-compose up -d

After this command two docker containers are spin and run

### To Access the application - 
Application can be accessed at http://localhost:9092

The application is made for courses. With a Post request you can add a course to the DB and with a get request you can retrieve the list of all courses.
(Postman collection is attached in the deliverables)

### To stop the app & remove the containers created by us
docker-compose down -v


# Running with Jenkins Pipeline and creating image on dockerhub
Run the pipeline on Jenkins using Jenkinsfile







